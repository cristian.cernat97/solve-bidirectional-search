var pathBFS = document.getElementById("pathFirstSearch");
//var pathBFS2 = document.getElementById("pathSecondSearch");

pathBFS.innerHTML = "Works1";
//pathBFS2.innerHTML = "Works2";
toShowPathBFS = "";

//Cities
var cities = [
    "Satu Mare", "Baia Mare", "Oradea", "Cluj-Napoca", "Arad",
    "Alba-Iulia", "Timisoara", "Craiova", "Pitesti", "Brasov",
    "Sibiu", "Targu Mures", "Bistrita", "Suceava", "Iasi",
    "Piatra Neamt", "Bucuresti", "Braila", "Tulcea", "Constanta"
];

//Adj matrix
var edges = matrix(20, 20); //a[][]
const nrCities = 20;
var i, j;

//BFS
var nodes = matrix(20, 20).fill(0), // matrix of nodes
    startCity = 0,
    destCity = 0,
    currentNode,

    sourceVisited = new Array(20).fill(0), //left source array
    destinationVisited = new Array(20).fill(0), // right source array

    sourceParent = new Array(20).fill(0), // left parent source array
    destinationParent = new Array(20).fill(0), // right parent source array

    sourceQueue = [],
    destinationQueue = [],

    intersectNode = -1,
    sourceCity = "",
    destinationCity = "";

console.log(edges);

//Satu Mare
edges[0][1] = 1;
edges[0][3] = 1;
//Baia Mare
edges[1][3] = 1;
//Oradea
edges[2][3] = 1;
edges[2][4] = 1;
//Cluj
edges[3][4] = 1;
edges[3][5] = 1;
edges[3][11] = 1;
edges[3][12] = 1;
//Arad
edges[4][6] = 1;
//Alba
edges[5][6] = 1;
edges[5][7] = 1;
edges[5][10] = 1;
//Timisoara
edges[6][7] = 1;
//Craiova
edges[7][8] = 1;
//Pitesti
edges[8][9] = 1;
edges[8][16] = 1
//Brasov
edges[9][10] = 1;
edges[9][11] = 1;
edges[9][15] = 1;
//Sibiu
edges[10][11] = 1;
//Targu Mures
edges[11][12] = 1;
//Bistrita
edges[12][13] = 1;
//Suceava
edges[13][14] = 1;
//Iasi
edges[14][15] = 1;
//Piara Neamt
edges[15][16] = 1;
edges[15][17] = 1;
//Bucuresti
edges[16][17] = 1;
edges[16][19] = 1;
//Braila
edges[17][18] = 1;
//Tulcea
edges[18][19] = 1;

//Inverse
for (i = 0; i < nrCities; i++) {
    for (j = 0; j < nrCities; j++) {
        if (edges[i][j] == 1)
            edges[j][i] = 1;
    }
}

sourceCity = prompt("Source city:", "Craiova");
destinationCity = prompt("Destination city:", "Bucuresti");

searchType1 = prompt("Write what will be the first search. Write DFS of BFS", "BFS");
searchType2 = prompt("Write what will be the first search. Write DFS of BFS", "DFS");
for (var i = 0; i < nrCities; i++) {
    if (sourceCity.localeCompare(cities[i]) == 0) {
        startCity = i;
    }
    if (destinationCity.localeCompare(cities[i]) == 0) {
        destCity = i;
    }
}

bidirectionalSearch(startCity, destCity);

pathBFS.innerHTML = toShowPathBFS;

//FUNCTIONS:
function searchCitiesBFS(id, queueL, visitedL, parentL) { // formal parameters but never used
    alert("BFS called");
    if (id == 1) {
        console.log("SourceQueue");
        console.log(sourceQueue);
        currentNode = sourceQueue.slice(0, 1);
        currentNode = parseInt(currentNode.join('')); //to int
        sourceQueue.shift();
        console.log("Start city1=", currentNode);
        //  console.log("Dest city=", sourceQueue.slice(-1)[0]);
    }
    if (id == 2) {
        console.log("SourceQueue2");
        console.log(destinationQueue);
        currentNode = destinationQueue.slice(0, 1);
        currentNode = parseInt(currentNode.join('')); // to int
        destinationQueue.shift();
        console.log("Start city2=", currentNode);
        // console.log("Dest city=", destinationQueue.slice(-1)[0]);
    }

    console.log(currentNode);
    for (i = 0; i < nrCities; i++)
        if (edges[currentNode][i] == 1) { //BFS1
            console.log("Current city:" + cities[currentNode]);
            if (id == 1)
                if (sourceVisited[i] == 0) { //search connected and unvisited
                    //alert("Push from 1");
                    sourceParent[i] = currentNode; //i child
                    sourceVisited[i] = 1;
                    sourceQueue.push(i);
                }
            if (id == 2)
                if (destinationVisited[i] == 0) { //BFS2
                    //alert("Push from 2");
                    destinationParent[i] = currentNode; //i child
                    destinationVisited[i] = 1;
                    destinationQueue.push(i);
                }

        }

}

function searchCitiesDFS(id, queueL, visitedL, parentL) { // formal parameters but never used
    alert("DFS called");
    if (id == 1) {
        console.log("SourceQueue");
        console.log(sourceQueue);
        currentNode = sourceQueue.slice(0, 1);
        currentNode = parseInt(currentNode.join('')); //to int
        sourceQueue.shift();
        console.log("Start city1=", currentNode);
        //  console.log("Dest city=", sourceQueue.slice(-1)[0]);
    }
    if (id == 2) {
        console.log("SourceQueue2");
        console.log(destinationQueue);
        currentNode = destinationQueue.slice(0, 1);
        currentNode = parseInt(currentNode.join('')); // to int
        destinationQueue.shift();
        console.log("Start city2=", currentNode);


        // console.log("Dest city=", destinationQueue.slice(-1)[0]);
    }

    console.log(currentNode);
    for (i = 0; i < nrCities; i++)
        if (edges[currentNode][i] == 1) { //BFS1
            console.log("Current city:" + cities[currentNode]);
            if (id == 1)
                if (sourceVisited[i] == 0) { //search connected and unvisited
                    //alert("Push from 1");
                    for (let i = 0; i > 0; i--)
                        sourceQueue[i] = sourceQueue[i - 1];
                    sourceParent[i] = currentNode; //i child
                    sourceVisited[i] = 1;
                    sourceQueue.push(i);
                }
            if (id == 2)
                if (destinationVisited[i] == 0) { //BFS2
                    //alert("Push from 2");
                    for (let i = 0; i > 0; i--)
                        destinationQueue[i] = destinationQueue[i - 1];

                    destinationParent[i] = currentNode; //i child
                    destinationVisited[i] = 1;
                    destinationQueue.push(i);
                }

        }

}


function matrix(m, n) {
    var result = []
    for (var i = 0; i < n; i++) {
        result.push(new Array(m).fill(0))
    }
    return result
}

function bidirectionalSearch(source, destination) {

    sourceQueue.push(source); // add the first node of the left source
    sourceVisited[source] = 1; // mark it as visited
    sourceParent[source] = -1; // mark it out of boundaries

    destinationQueue.push(destination); // add the first node of the right source
    destinationVisited[destination] = 1; // mark it as visited
    destinationParent[destination] = -1; // mark it out of boundaries

    //check if is empty or not defined 
    while ((typeof sourceQueue !== 'undefined' && sourceQueue.length > 0) &&
        (typeof destinationQueue !== 'undefined' && destinationQueue.length > 0)) {

        // call till queues are intersecting
        if (searchType1.localeCompare("BFS") == 0)
            searchCitiesBFS(1, sourceQueue, sourceVisited, sourceParent);
        else
            searchCitiesDFS(1, sourceQueue, sourceVisited, sourceParent);

        if (searchType2.localeCompare("DFS") == 0)
            searchCitiesDFS(2, destinationQueue, destinationVisited, destinationParent);
        else
            searchCitiesBFS(2, destinationQueue, destinationVisited, destinationParent);
        console.log("Visited");
        console.log(sourceVisited);
        console.log(destinationVisited);
        console.log("Queues");
        toShowPathBFS += "Source " + searchType1 + "cities=" + sourceQueue.length + "<br>";
        toShowPathBFS += "Source " + searchType2 + "cities=" + destinationQueue.length + "<br>";
        console.log(sourceQueue);
        console.log(destinationQueue);
        // check intersection value
        intersectNode = (() => {
            var intersectNode = -1;
            for (var i = 0; i < 20; i++) {
                if (sourceVisited[i] && destinationVisited[i]) // intersection check
                    return i;
            }
            return -1;
        })(); //autocall function


        //show
        if (intersectNode != -1) {
            alert("Path exist between " + source + " and " + destination);
            alert("Intersection at: " + intersectNode);

            // print the path and exit the program
            showSolution(sourceParent, destinationParent, source, destination, intersectNode);
            break;
        }
    }
}

function showSolution(sourceParentL, destinationParentL, sourceL, destinationL, intersectNodeL) {
    var path = [],
        index = intersectNodeL;

    path.push(intersectNodeL);

    while (index != sourceL) { // while did arrieved at the index
        path.push(sourceParentL[index]);
        index = sourceParentL[index];
    }

    path.reverse(); // reverse the path for checking
    index = intersectNodeL;

    while (index != destinationL) { // while did arrieved at the index
        path.push(destinationParentL[index]);
        index = destinationParentL[index];
    }
    console.log(path);
    //show each element
    path.forEach((item) => {
        toShowPathBFS += cities[item] + " ";
    });
};