# Project Title

Solves bidrectional search in graphs using double BFS or DFS (you can choose either of them)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

A browser is needed. I recommend Chrome.

```
https://www.google.com/chrome/
```

### Installing

Download the file for your system with the name bidirectionalsearch-win.exe for example for Windows

or using git

```
 git clone https://gitlab.com/cristian.cernat97/solve-bidirectional-search
```
* Windows - bidirectionalsearch-win.exe
* MacOS - bidirectionalsearch-macos
* Linux - bidirectionalsearch-linux

```
1. Launch the file;
2. Search on browser localhost:8080;
3. Enjoy, choose what search do you want to use DFS or BFS.
```
## Built With

* [PKG](https://www.npmjs.com/package/pkg) - Packager
* [Express](https://www.npmjs.com/package/express) - Server
* Pure JavaScript, HTML5 and CSS3

## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.0 

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)